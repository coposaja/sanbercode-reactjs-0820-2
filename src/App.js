import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Tugas9 from './Tugas-9/Tugas9';
import Tugas10 from './Tugas-10/Tugas10';
import Tugas11 from './Tugas-11/Tugas11';
import Tugas12 from './Tugas-12/Tugas12';
import Tugas13 from './Tugas-13/Tugas13';
import Tugas14 from './Tugas-14/Tugas14';
import Tugas15 from './Tugas-15/Tugas15';
import Navbar from './Navbar/Navbar';
import { ApplicationProvider } from './ApplicationContext';

function App() {
  return (
    <BrowserRouter>
      <ApplicationProvider>
        <Navbar />
        <div className="container">
          <Switch>
            <Route path="/Tugas-9" component={Tugas9} />
            <Route path="/Tugas-10" component={Tugas10} />
            <Route path="/Tugas-11" component={Tugas11} />
            <Route path="/Tugas-12" component={Tugas12} />
            <Route path="/Tugas-13" component={Tugas13} />
            <Route path="/Tugas-14" component={Tugas14} />
            <Route path="/Tugas-15" component={Tugas15} />
          </Switch>
        </div>
      </ApplicationProvider>
    </BrowserRouter>
  );
}

export default App;
