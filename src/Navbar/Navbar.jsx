import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { ApplicationContext } from '../ApplicationContext';

const Navbar = () => {
  const [theme] = useContext(ApplicationContext);

  return (
    <nav className={theme}>
      <ul>
        <li>
          <NavLink to="/Tugas-9">Tugas 9</NavLink>
        </li>
        <li>
          <NavLink to="/Tugas-10">Tugas 10</NavLink>
        </li>
        <li>
          <NavLink to="/Tugas-11">Tugas 11</NavLink>
        </li>
        <li>
          <NavLink to="/Tugas-12">Tugas 12</NavLink>
        </li>
        <li>
          <NavLink to="/Tugas-13">Tugas 13</NavLink>
        </li>
        <li>
          <NavLink to="/Tugas-14">Tugas 14</NavLink>
        </li>
        <li>
          <NavLink to="/Tugas-15">Tugas 15</NavLink>
        </li>
      </ul>
    </nav>
  )
}

export default Navbar;
