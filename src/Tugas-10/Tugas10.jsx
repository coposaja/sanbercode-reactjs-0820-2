import React, { Component } from 'react';

let dataHargaBuah = [
  { nama: "Semangka", harga: 10000, berat: 1000 },
  { nama: "Anggur", harga: 40000, berat: 500 },
  { nama: "Strawberry", harga: 30000, berat: 400 },
  { nama: "Jeruk", harga: 30000, berat: 1000 },
  { nama: "Mangga", harga: 30000, berat: 500 }
]

class Tugas10 extends Component {
  render() {
    return (
      <div className="centered">
        <h1>Tabel Harga Buah</h1>
        <table className="tugas10--table">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
            </tr>
          </thead>
          <tbody>
            {dataHargaBuah.map((data, index) => (
              <tr key={index}>
                <td>{data.nama}</td>
                <td>{data.harga}</td>
                <td>{data.berat / 1000} kg</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default Tugas10
