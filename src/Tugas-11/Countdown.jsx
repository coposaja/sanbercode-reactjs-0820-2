import React, { Component } from 'react'

class Countdown extends Component {
  constructor() {
    super();
    this.state = {
      timer: 100,
      timerId: 0,
    }
  }

  componentDidMount() {
    const id = setInterval(this.ticking, 1000);
    this.setState({
      timerId: id,
    })
  }

  componentDidUpdate() {
    if (this.state.timer <= 0) this.props.onTimerStopHandler();
  }

  componentWillUnmount() {
    clearInterval(this.state.timerId);
  }

  ticking = () => {
    this.setState({
      timer: this.state.timer - 1
    })
  }

  render() {
    return (
      <h1>hitung mundur: {this.state.timer}</h1>
    )
  }
}

export default Countdown
