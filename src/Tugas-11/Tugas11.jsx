import React, { Component } from 'react'
import Countdown from './Countdown'
import Time from './Time'

class Tugas11 extends Component {
  constructor() {
    super();
    this.state = {
      isActive: true
    }
  }

  onTimerStopHandler = () => {
    this.setState({ isActive: false })
  }

  render() {
    return (
      this.state.isActive ? (
        <div className="tugas11--time-wrapper">
          <Time />
          <Countdown onTimerStopHandler={this.onTimerStopHandler} />
        </div>
      ) : null
    )
  }
}

export default Tugas11
