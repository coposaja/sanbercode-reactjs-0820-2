import React, { Component } from 'react'

class Time extends Component {
  constructor() {
    super();
    this.state = {
      currTime: new Date().toLocaleTimeString('en-US'),
      intervalId: 0
    }
  }

  componentDidMount() {
    const id = setInterval(this.ticking, 1000);
    this.setState({
      intervalId: id,
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  ticking = () => {
    this.setState({
      currTime: new Date().toLocaleTimeString('en-US')
    })
  }

  render() {
    return (
      <h1>sekarang jam: {this.state.currTime}</h1>
    )
  }
}

export default Time
