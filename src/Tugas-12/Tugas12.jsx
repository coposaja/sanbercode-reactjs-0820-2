import React, { Component } from 'react';

class Tugas12 extends Component {
  constructor() {
    super();
    this.state = {
      fruitList: [
        { nama: "Semangka", harga: 10000, berat: 1000 },
        { nama: "Anggur", harga: 40000, berat: 500 },
        { nama: "Strawberry", harga: 30000, berat: 400 },
        { nama: "Jeruk", harga: 30000, berat: 1000 },
        { nama: "Mangga", harga: 30000, berat: 500 },
      ],
      fruitName: '',
      fruitPrice: 0,
      fruitWeight: 0,
      fruitIndex: -1,
    }
  }

  onDeleteHandler = (index) => {
    this.setState({
      fruitList: this.state.fruitList.filter((_, idx) => idx !== index)
    })
  }

  onPriceChangeHandler = (e) => {
    const price = isNaN(e.target.value) ? 0 : +e.target.value;
    this.setState({
      fruitPrice: price
    });
  }

  onWeightChangeHandler = (e) => {
    const weight = isNaN(e.target.value) ? 0 : +e.target.value;
    this.setState({
      fruitWeight: weight
    })
  }

  onInsertHandler = () => {
    const { fruitName, fruitPrice, fruitWeight } = this.state;
    const fruit = { nama: fruitName, harga: fruitPrice, berat: fruitWeight };
    this.setState({
      fruitList: [...this.state.fruitList, fruit],
      fruitName: '',
      fruitPrice: 0,
      fruitWeight: 0,
    })
  }

  onEditHandler = (index) => {
    const fruit = this.state.fruitList.find((_, idx) => idx === index)
    this.setState({
      fruitName: fruit.nama,
      fruitPrice: fruit.harga,
      fruitWeight: fruit.berat,
      fruitIndex: index,
    })
  }

  onSaveHandler = () => {
    const { fruitName, fruitPrice, fruitWeight, fruitIndex, fruitList } = this.state;
    const fruit = { nama: fruitName, harga: fruitPrice, berat: fruitWeight };

    const updatedFruitList = fruitList.map((x, index) => {
      if (index === fruitIndex) {
        return {
          ...x,
          ...fruit
        }
      };
      return x;
    })
    this.setState({
      fruitList: updatedFruitList,
      fruitName: '',
      fruitPrice: 0,
      fruitWeight: 0,
      fruitIndex: -1,
    })
  }

  render() {
    const {
      fruitList,
      fruitName,
      fruitPrice,
      fruitWeight,
      fruitIndex,
    } = this.state;

    return (
      <div className="centered">
        <h1>Tabel Harga Buah</h1>
        <table className="tugas10--table">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th style={{ width: '100px' }}>Action</th>
            </tr>
          </thead>
          <tbody>
            {fruitList.length ? (
              fruitList.map((data, index) => (
                <tr key={index}>
                  <td>{data.nama}</td>
                  <td>{data.harga}</td>
                  <td>{data.berat / 1000} kg</td>
                  <td>
                    <button onClick={() => this.onEditHandler(index)}>Edit</button>
                    <button onClick={() => this.onDeleteHandler(index)}>Delete</button>
                  </td>
                </tr>
              ))
            ) : <tr><td colSpan={4} style={{ textAlign: 'center', fontWeight: 'bold' }}>No Data</td></tr>}

            <tr>
              <td>
                <input
                  placeholder="Nama Buah"
                  value={fruitName}
                  onChange={(e) => this.setState({ fruitName: e.target.value })}
                />
              </td>
              <td>
                <input
                  placeholder="Harga Buah"
                  value={+fruitPrice}
                  onChange={this.onPriceChangeHandler}
                />
              </td>
              <td>
                <div style={{ position: 'relative' }}>
                  <input
                    placeholder="Berat Buah (gr)"
                    value={+fruitWeight}
                    onChange={this.onWeightChangeHandler}
                  />
                  <span style={{ position: 'absolute', right: '5px', top: '5px' }}><b>gr</b></span>
                </div>
              </td>
              <td>
                {
                  fruitIndex >= 0 ?
                    <button onClick={this.onSaveHandler}>Save</button> :
                    <button onClick={this.onInsertHandler}>Insert</button>
                }
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default Tugas12
