import React, { useState, useEffect } from 'react';
import Axios from 'axios';

const Tugas13 = () => {
  const [fruitList, setFruitList] = useState([]);
  const [fruitName, setFruitName] = useState('');
  const [fruitPrice, setFruitPrice] = useState(0);
  const [fruitWeight, setFruitWeight] = useState(0);
  const [fruitId, setFruitId] = useState(-1);

  useEffect(() => {
    requestData();
  }, [])

  const requestData = () => {
    Axios.get('http://backendexample.sanbercloud.com/api/fruits')
      .then((res) => setFruitList(res.data))
      .catch((err) => console.log(err))
  }

  const onDeleteHandler = (id) => {
    Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
      .then((_) => requestData())
      .catch((err) => console.log(err))
  }

  const onPriceChangeHandler = (e) => {
    const price = isNaN(e.target.value) ? 0 : +e.target.value;
    setFruitPrice(price);
  }

  const onWeightChangeHandler = (e) => {
    const weight = isNaN(e.target.value) ? 0 : +e.target.value;
    setFruitWeight(weight);
  }

  const onInsertHandler = () => {
    const fruit = { name: fruitName, price: fruitPrice, weight: fruitWeight };
    Axios.post('http://backendexample.sanbercloud.com/api/fruits', fruit)
      .then((_) => {
        requestData();
        onFormReset();
      })
      .catch((err) => console.log(err))
  }

  const onEditHandler = (id) => {
    const fruit = fruitList.find((x) => x.id === id)
    setFruitName(fruit.name)
    setFruitPrice(fruit.price)
    setFruitWeight(fruit.weight)
    setFruitId(id)
  }

  const onSaveHandler = () => {
    const fruit = { name: fruitName, price: fruitPrice, weight: fruitWeight };

    Axios.put(`http://backendexample.sanbercloud.com/api/fruits/${fruitId}`, fruit)
      .then((_) => {
        requestData()
        onFormReset();
      })
      .catch((err) => console.log(err))
  }

  const onFormReset = () => {
    setFruitName('');
    setFruitPrice(0);
    setFruitWeight(0);
    setFruitId(-1);
  }

  return (
    <div className="centered">
      <h1>Tabel Harga Buah</h1>
      <table className="tugas10--table">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th style={{ width: '100px' }}>Action</th>
          </tr>
        </thead>
        <tbody>
          {fruitList.length ? (
            fruitList.map((data) => (
              <tr key={data.id}>
                <td>{data.name}</td>
                <td>{data.price}</td>
                <td>{data.weight / 1000} kg</td>
                <td>
                  <button onClick={() => onEditHandler(data.id)}>Edit</button>
                  <button onClick={() => onDeleteHandler(data.id)}>Delete</button>
                </td>
              </tr>
            ))
          ) : <tr><td colSpan={4} style={{ textAlign: 'center', fontWeight: 'bold' }}>No Data</td></tr>}

          <tr>
            <td>
              <input
                placeholder="Nama Buah"
                value={fruitName}
                onChange={(e) => setFruitName(e.target.value)}
              />
            </td>
            <td>
              <input
                placeholder="Harga Buah"
                value={+fruitPrice}
                onChange={onPriceChangeHandler}
              />
            </td>
            <td>
              <div style={{ position: 'relative' }}>
                <input
                  placeholder="Berat Buah (gr)"
                  value={+fruitWeight}
                  onChange={onWeightChangeHandler}
                />
                <span style={{ position: 'absolute', right: '5px', top: '5px' }}><b>gr</b></span>
              </div>
            </td>
            <td>
              {
                fruitId >= 0 ?
                  <button onClick={onSaveHandler}>Save</button> :
                  <button onClick={onInsertHandler}>Insert</button>
              }
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default Tugas13
