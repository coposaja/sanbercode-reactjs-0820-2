import React, { useEffect, useContext } from 'react';
import Axios from 'axios';
import { FruitContext } from './FruitContext';

const FruitTable = () => {
  const [fruitList, setFruitList, , setSelectedFruit] = useContext(FruitContext);

  useEffect(() => {
    requestData();
  }, [])

  const requestData = () => {
    Axios.get('http://backendexample.sanbercloud.com/api/fruits')
      .then((res) => setFruitList(res.data))
      .catch((err) => console.log(err))
  }

  const onDeleteHandler = (id) => {
    Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
      .then((_) => requestData())
      .catch((err) => console.log(err))
  }

  const onEditHandler = (id) => {
    const fruit = fruitList.find((x) => x.id === id)
    setSelectedFruit(fruit);
  }

  return (
    <>
      <h1>Tabel Harga Buah</h1>
      <table className="tugas10--table">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th style={{ width: '100px' }}>Action</th>
          </tr>
        </thead>
        <tbody>
          {fruitList.length ? (
            fruitList.map((data) => (
              <tr key={data.id}>
                <td>{data.name}</td>
                <td>{data.price}</td>
                <td>{data.weight / 1000} kg</td>
                <td>
                  <button onClick={() => onEditHandler(data.id)}>Edit</button>
                  <button onClick={() => onDeleteHandler(data.id)}>Delete</button>
                </td>
              </tr>
            ))
          ) : <tr><td colSpan={4} style={{ textAlign: 'center', fontWeight: 'bold' }}>No Data</td></tr>}
        </tbody>
      </table>
    </>
  )
}

export default FruitTable;
