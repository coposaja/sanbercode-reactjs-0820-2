import React from 'react';
import FruitTable from './FruitTable';
import { FruitProvider } from './FruitContext';
import FruitForm from './FruitForm';

const Tugas14 = () => {
  return (
    <div className="centered">
      <FruitProvider>
        <FruitTable />
        <FruitForm />
      </FruitProvider>
    </div>
  )
}

export default Tugas14
