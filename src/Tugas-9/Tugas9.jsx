import React from 'react';

export default function Tugas9() {
  return (
    <div className="content">
      <h1 className="centered">Form Pembelian Buah</h1>

      <form>
        <div className="form-group">
          <div className="form-label"><label htmlFor="txtName">Nama Pelanggan</label></div>
          <input type="text" id="txtName" />
        </div>

        <div className="form-group">
          <div className="form-label"><label>Daftar Item</label></div>
          <div className="daftar-item">
            <div className="item">
              <input type="checkbox" id="cbSemangka" /> <label htmlFor="cbSemangka">Semangka</label>
            </div>
            <div className="item">
              <input type="checkbox" id="cbJeruk" /> <label htmlFor="cbJeruk">Jeruk</label>
            </div>
            <div className="item">
              <input type="checkbox" id="cbNanas" /> <label htmlFor="cbNanas">Nanas</label>
            </div>
            <div className="item">
              <input type="checkbox" id="cbSalak" /> <label htmlFor="cbSalak">Salak</label>
            </div>
            <div className="item">
              <input type="checkbox" id="cbAnggur" /> <label htmlFor="cbAnggur">Anggur</label>
            </div>
          </div>
        </div>

        <button>Kirim</button>
      </form>
    </div>
  )
}
