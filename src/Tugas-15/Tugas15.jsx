import React, { useContext } from 'react'
import { ApplicationContext } from '../ApplicationContext'

const Tugas15 = () => {
  const [theme, setTheme] = useContext(ApplicationContext);

  return (
    <div className="centered">
      <button
        onClick={() => {
          setTheme(theme === "dark" ? "light" : "dark")
        }}
      >Change Theme</button>
    </div>
  )
}

export default Tugas15
